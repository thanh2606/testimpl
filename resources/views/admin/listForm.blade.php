@extends('admin.layout.master')

@section('title')
    Seller manager
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Seller manager</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Seller manager</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cart-table" id="dataFormSeller">
                            @include('admin.table.customerFormSeller')
                        </div>
                    </div>
                </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="{{asset('sweetAlert\sweetalert2.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('sweetAlert\sweetalert2.min.css')}}">
    <script src="{{asset('js\customer_form_seller.js')}}"></script>
@endsection
