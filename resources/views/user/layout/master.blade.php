<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/themify-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/nice-select.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/jquery-ui.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }}" type="text/css">
</head>

<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Header Section Begin -->
    @include('user.layout.header')
<!-- Header End -->

@yield('content')

<!-- Footer Section Begin -->
@include('user.layout.footer')
<!-- Footer Section End -->

<!-- Js Plugins -->
<script src="{{ asset('customer/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery.zoom.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery.dd.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery.slicknav.js') }}"></script>
<script src="{{ asset('customer/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('customer/js/main.js') }}"></script>
<script src="{{ asset('js/baseJs.js') }}"></script>
@yield('script')
</body>

</html>
