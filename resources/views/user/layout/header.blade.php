<header class="header-section">
    <div class="container">
        <div class="inner-header">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    @if(\Illuminate\Support\Facades\Session::get('role') == \App\User::ADMIN_ROLE && \Illuminate\Support\Facades\Session::has('role'))
                        <div class="logo">
                            <ul class="nav-right">
                                <li class="heart-icon">
                                    <a href="{{ route('admin.dashboard') }}">
                                        Admin dashboard
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @else
                        <div class="logo">
                            <a href="{{ route('index') }}">
                                <img src="{{ asset('customer/img/logo.png') }}" alt="">
                            </a>
                        </div>
                    @endif
                </div>
                <div class="col-lg-6 text-right col-md-6">
                    <ul class="nav-right">
                        @if(\Illuminate\Support\Facades\Session::exists('isLogin'))
                            <li class="heart-icon">
                                {{ \Illuminate\Support\Facades\Session::get('name') }}
                            </li>
                            <li class="heart-icon">
                                <a href="{{ route('auth.logout') }}">
                                    <i class="fas fa-sign-out-alt"></i>
                                    Logout
                                </a>
                            </li>
                            @if(\Illuminate\Support\Facades\Session::get('role') != \App\User::ADMIN_ROLE && Request::route()->getName() != 'customer.indexForm')
                                <li class="heart-icon">
                                    <a href="{{ route('customer.indexForm') }}">
                                        <i class="fas fa-sign-out-alt"></i>
                                        You want to be seller?
                                    </a>
                                </li>
                            @endif
                        @endif
                        @if(! \Illuminate\Support\Facades\Session::exists('isLogin'))
                            <li class="heart-icon">
                                <a href="{{ route('auth.get.login') }}">
                                    Login
                                </a>
                            </li>
                            <li class="heart-icon">
                                <a href="{{ route('auth.get.register') }}">
                                    Register
                                </a>
                            </li>
                        @endif
                        <li class="cart-icon">
                            <a href="#">
                                <i class="icon_bag_alt"></i>
                                <span>3</span>
                            </a>
                            <div class="cart-hover">
                                <div class="select-items">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td class="si-pic"><img src="{{ asset('customer/img/select-product-1.jpg') }}" alt=""></td>
                                            <td class="si-text">
                                                <div class="product-selected">
                                                    <p>$60.00 x 1</p>
                                                    <h6>Kabino Bedside Table</h6>
                                                </div>
                                            </td>
                                            <td class="si-close">
                                                <i class="ti-close"></i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="si-pic"><img src="{{ asset('customer/img/select-product-2.jpg') }}" alt=""></td>
                                            <td class="si-text">
                                                <div class="product-selected">
                                                    <p>$60.00 x 1</p>
                                                    <h6>Kabino Bedside Table</h6>
                                                </div>
                                            </td>
                                            <td class="si-close">
                                                <i class="ti-close"></i>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="select-total">
                                    <span>total:</span>
                                    <h5>$120.00</h5>
                                </div>
                                <div class="select-button">
                                    <a href="#" class="primary-btn view-card">VIEW CARD</a>
                                    <a href="#" class="primary-btn checkout-btn">CHECK OUT</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-item">
        <div class="container">
            <div class="nav-depart">
                <div class="depart-btn">
                    <i class="ti-menu"></i>
                    <span>All departments</span>
                    <ul class="depart-hover">
                        <li class="active"><a href="#">Women’s Clothing</a></li>
                        <li><a href="#">Men’s Clothing</a></li>
                        <li><a href="#">Underwear</a></li>
                        <li><a href="#">Kid's Clothing</a></li>
                        <li><a href="#">Brand Fashion</a></li>
                        <li><a href="#">Accessories/Shoes</a></li>
                        <li><a href="#">Luxury Brands</a></li>
                        <li><a href="#">Brand Outdoor Apparel</a></li>
                    </ul>
                </div>
            </div>
            <nav class="nav-menu mobile-menu">
                <ul>
                    <li><a href="./index.html">Home</a></li>
                    <li><a href="./shop.html">Shop</a></li>
                    <li><a href="#">Collection</a>
                        <ul class="dropdown">
                            <li><a href="#">Men's</a></li>
                            <li><a href="#">Women's</a></li>
                            <li><a href="#">Kid's</a></li>
                        </ul>
                    </li>
                    <li><a href="./blog.html">Blog</a></li>
                    <li><a href="./contact.html">Contact</a></li>
                    <li><a href="#">Pages</a>
                        <ul class="dropdown">
                            <li><a href="./blog-details.html">Blog Details</a></li>
                            <li><a href="./shopping-cart.html">Shopping Cart</a></li>
                            <li><a href="./check-out.html">Checkout</a></li>
                            <li><a href="./faq.html">Faq</a></li>
                            <li><a href="./register.html">Register</a></li>
                            <li><a href="./login.html">Login</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <div id="mobile-menu-wrap"></div>
        </div>
    </div>
</header>
