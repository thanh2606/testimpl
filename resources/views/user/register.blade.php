@extends('user.layout.master')

@section('title')
    Register
@endsection

@section('content')
    <!-- Register Section Begin -->
    <div class="register-login-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="register-form">
                        <h2>Register</h2>
                        <form action="{{ route('auth.post.register') }}" method="post">
                            @csrf
                            <div class="group-input">
                                <label for="username">User name *</label>
                                <input type="text" name="name" value="{{ old('name') }}">
                                @error('name')
                                <b style="color: red">{{ $message }}</b>
                                @enderror
                            </div>
                            <div class="group-input">
                                <label for="username">Email address *</label>
                                <input type="text" name="email" value="{{ old('email') }}">
                                @error('email')
                                    <b style="color: red">{{ $message }}</b>
                                @enderror
                            </div>
                            <div class="group-input">
                                <label for="pass">Password *</label>
                                <input type="password" name="password" value="{{ old('password') }}">
                                @error('password')
                                <b style="color: red">{{ $message }}</b>
                                @enderror
                            </div>
                            <div class="group-input">
                                <label for="con-pass">Confirm Password *</label>
                                <input type="password" name="password_confirmation">
                                @error('password_confirmation')
                                <b style="color: red">{{ $message }}</b>
                                @enderror
                            </div>
                            <button type="submit" class="site-btn register-btn">REGISTER</button>
                        </form>
                        <div class="switch-login">
                            <a href="{{ route('auth.get.login') }}" class="or-login">Or Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Form Section End -->
@endsection
