@extends('user.layout.master')

@section('title')
    Form register to be seller
@endsection

@section('content')
    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ route('customer.get.form') }}" type="button" class="btn btn-primary" style="color: white; margin-bottom: 20px">New form</a>
                    <div class="cart-table" id="dataFormSeller">
                        @include('user.table.customerFormSeller')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->
@endsection

@section('script')
    <script src="{{asset('sweetAlert\sweetalert2.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('sweetAlert\sweetalert2.min.css')}}">
    <script src="{{asset('js\customer_form_seller.js')}}"></script>
@endsection
