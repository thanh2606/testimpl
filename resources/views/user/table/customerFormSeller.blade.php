<table>
    <thead>
    <tr>
        <th>STT</th>
        <th>Shop name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <?php $dem = 1; ?>
    @foreach($forms as $form)
        <tr>
            <td>{{ $dem }}</td>
            <td>
                {{ $form->shop_name }}
            </td>
            <td>{{ $form->shop_email }}</td>
            <td>{{ $form->shop_address }}</td>
            @if($form->status == \App\Form::WAITING)
                <td>
                    <button type="button" class="btn btn-warning" disabled style="margin: 5px">Waiting</button>
                </td>
            @endif
            @if($form->status == \App\Form::CONFIRM)
                <td>
                    <button type="button" class="btn btn-success" disabled style="margin: 5px">Success</button>
                </td>
            @endif
            @if($form->status == \App\Form::REJECT)
                <td>
                    <button type="button" class="col-lg-6 btn btn-danger btn-reject" data-id = '{{$form->id}}' style="margin: 5px">Retry</button>
                </td>
            @endif
        </tr>
        <?php $dem++ ?>
    @endforeach
    </tbody>
</table>
<div style="margin-top: 20px">{{ $forms->links() }}</div>
