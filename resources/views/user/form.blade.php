@extends('user.layout.master')

@section('title')
    Form register to be seller
@endsection

@section('content')
<!-- Contact Section Begin -->
<section class="contact-section spad"   >
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 offset-lg-2">
                <div class="contact-title">
                    <h4>Shops information</h4>
                </div>
                <div class="contact-form">
                    <div class="leave-comment">
                        <form action="{{ route('customer.post.form') }}" method="post" class="comment-form">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    @error('shop_name')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                    <input type="text" name="shop_name" placeholder="Name" value="{{ old('shop_name') }}">
                                </div>
                                <div class="col-lg-6">
                                    @error('shop_email')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                    <input type="text" name="shop_email" placeholder="Email" value="{{ old('shop_email') }}">
                                </div>
                                <div class="col-lg-6">
                                    @error('telephone_number')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                    <input type="text" name="telephone_number" placeholder="Telephone number" value="{{ old('telephone_number') }}">
                                </div>
                                <div class="col-lg-6">
                                    @error('shop_address')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                    <input type="text" name="shop_address" placeholder="Address"  value="{{ old('shop_address') }}">
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="description" placeholder="About shop"></textarea>
                                    <button type="submit" class="site-btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->
@endsection
