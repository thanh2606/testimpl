<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    const WAITING = 0;
    const CONFIRM = 1;
    const REJECT = 2;

    const PAGINATE = 10;

    protected $fillable = [
        'user_id',
        'shop_name',
        'description',
        'telephone_number',
        'shop_address',
        'shop_email',
        'status'
    ];

    public function getFormsBy($user_id)
    {
        return $this->where('user_id', $user_id)->latest()->paginate($this::PAGINATE);
    }

    public function getAll()
    {
        return $this->latest()->paginate($this::PAGINATE);
    }
}
