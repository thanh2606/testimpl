<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckRememberToken
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rememberToken = $request->cookie('rememberToken');
        $user = $this->user->findBy($rememberToken);
        if (!empty($user)) {
            $role = $user->role;
            $name = $user->name;
            $id = $user->id;
            $request->session()->put('role', $role);
            $request->session()->put('isLogin', true);
            $request->session()->put('name', $name);
            $request->session()->put('userId', $id);
            if ($role == User::ADMIN_ROLE) {
                return redirect()->route('admin.dashboard');
            } else {
                return redirect()->route('customer.index');
            }
        } else {
            return $next($request);
        }
    }
}
