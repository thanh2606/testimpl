<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route()->named('auth.get.register') || $request->route()->named('auth.post.register')) {
            if (!$request->session()->has('isLogin')) {
                return $next($request);
            } else {
                $role = $request->session()->get('role');
                if ($role == User::ADMIN_ROLE) {
                    return redirect()->route('admin.dashboard');
                } else {
                    return redirect()->route('customer.index');
                }
            }
        } else {
            if ($request->session()->has('isLogin')) {
                return $next($request);
            } else {
                return redirect()->route('auth.get.login');
            }
        }
    }
}
