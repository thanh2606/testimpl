<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginPost;
use App\Http\Requests\RegisterPost;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        if (!Session::has('isLogin')) {
            return view('auth.login');
        } elseif (Session::get('role') == User::ADMIN_ROLE) {
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->route('customer.index');
        }
    }

    public function login(LoginPost $request)
    {
        $email = $request->email;
        $password = $request->password;
        $user = $this->user->getBy($email);
        if (empty($user)) {
            return back()->with('emailError', 'Email does not exit');
        } else {
            if (Hash::check($password, $user->password)) {
                $role = $user->role;
                $name = $user->name;
                $id = $user->id;
                $rememberToken = null;
                $request->session()->put('role', $role);
                $request->session()->put('isLogin', true);
                $request->session()->put('name', $name);
                $request->session()->put('userId', $id);
                if(isset($request->remember)) {
                    $rememberToken = $id . hash('sha256', Str::random(32));
                    $this->user->updateRememberTokenBy($id, $rememberToken);
                }
                if ($role == User::ADMIN_ROLE) {
                    return redirect()->route('admin.dashboard')->withCookie('rememberToken', $rememberToken,7*24*60);
                } else {
                    return redirect()->route('customer.index')->withCookie('rememberToken', $rememberToken, 7*24*60);
                }
            } else {
                return back()->with('passwordError', 'Password is not true');
            }
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect()->route('customer.index')->withCookie('rememberToken', null, 0);
    }

    public function register()
    {
        return view('user/register');
    }

    public function store(RegisterPost $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $this->user::CUSTOMER_ROLE
        ];
        $user = $this->user->create($data);
        $name = $user->name;
        $id = $user->id;
        $request->session()->put('role', $this->user::CUSTOMER_ROLE);
        $request->session()->put('isLogin', true);
        $request->session()->put('name', $name);
        $request->session()->put('userId', $id);

        return redirect()->route('customer.index');
    }
}
