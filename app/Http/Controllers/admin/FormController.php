<?php

namespace App\Http\Controllers\admin;

use App\Form;
use App\Http\Controllers\Controller;
use App\Http\Requests\FormSellerPost;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FormController extends Controller
{
    protected $form, $user;

    public function __construct(Form $form, User $user)
    {
        $this->form = $form;
        $this->user = $user;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $forms = $this->form->getAll();
        return view('admin.listForm', ['forms' => $forms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormSellerPost $request)
    {
        $data = [
            'user_id' => $request->session()->get('userId'),
            'shop_name' => $request->shop_name,
            'shop_email' => $request->shop_email,
            'shop_address' => $request->shop_address,
            'telephone_number' => $request->telephone_number,
            'description' => $request->description,
            'status' => Form::WAITING,
        ];
        $this->form->create($data);
        return redirect()->route('customer.indexForm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $form = $this->form->findOrFail($id);
        $form->update($request->all());
        $forms = $this->form->getAll();
        if ($request->status == $this->form::CONFIRM) {
            $this->user->updateRoleBy($form->user_id, $this->user::SELLER_ROLE);
        }
        return view('admin.table.customerFormSeller', ['forms' => $forms]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
