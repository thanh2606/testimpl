<?php

namespace App\Http\Controllers\customer;

use App\Form;
use App\Http\Controllers\Controller;
use App\Http\Requests\FormSellerPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FormController extends Controller
{
    protected $form;

    public function __construct(Form $form)
    {
        $this->form = $form;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user_id = Session::get('userId');
        $forms = $this->form->getFormsBy($user_id);
        if (count($forms) > 0) {
            return view('user.listForm', ['forms' => $forms]);
        } else {
            return view('user.form');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormSellerPost $request)
    {
        $data = [
            'user_id' => $request->session()->get('userId'),
            'shop_name' => $request->shop_name,
            'shop_email' => $request->shop_email,
            'shop_address' => $request->shop_address,
            'telephone_number' => $request->telephone_number,
            'description' => $request->description,
            'status' => Form::WAITING,
        ];
        $this->form->create($data);
        return redirect()->route('customer.indexForm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $form = $this->form->findOrFail($id);
        $form->update($request->all());
        $user_id = Session::get('userId');
        $forms = $this->form->getFormsBy($user_id);
        return view('user.table.customerFormSeller', ['forms' => $forms]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
