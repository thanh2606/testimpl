<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index(Request $request)
    {
        $rememberToken = $request->cookie('rememberToken');
        $user = $this->user->findBy($rememberToken);
        if (!empty($user)) {
            $role = $user->role;
            $name = $user->name;
            $request->session()->put('role', $role);
            $request->session()->put('isLogin', true);
            $request->session()->put('name', $name);
        }
        return view('user.index');
    }
}
