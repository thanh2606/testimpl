<?php

namespace App\Http\Requests;

use App\Rules\IsTelephoneNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class FormSellerPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Session::has('isLogin')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_name' => 'required|unique:forms,shop_name',
            'shop_address' => 'required',
            'shop_email' => 'required| email|unique:forms,shop_email',
            'telephone_number' => ['required', new IsTelephoneNumber()],
        ];
    }
}
