<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    const ADMIN_ROLE = 0;
    const CUSTOMER_ROLE = 1;
    const SELLER_ROLE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'remember_token', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $email
     * @return mixed
     */
    public function getBy($email)
    {
        return $this->where('email', $email)->first();
    }

    /**
     * @param $id
     * @param $newToken
     */
    public function updateRememberTokenBy($id,$newToken)
    {
        $user = $this->findOrFail($id);
        $user->update(['remember_token' => $newToken]);
    }

    public function updateRoleBy($id,$newRole)
    {
        $user = $this->findOrFail($id);
        $user->update(['role' => $newRole]);
    }

    public function findBy($rememberToken)
    {
        return $this->where('remember_token', $rememberToken)->first();
    }
}
