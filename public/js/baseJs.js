$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function callAjax(url, method, data, type) {
    return $.ajax({
        url: url,
        type: method,
        data: data,
        dataType: type
    });
}
