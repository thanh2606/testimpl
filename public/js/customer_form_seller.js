$(document).on('click', '.btn-reject', function (event) {
    let id = $(this).attr('data-id');
    let data = '{ "status" : 0}';
    Swal.fire(
        'Are you sure?',
        'This form will resend to confirm by admin?',
        'question'
    ).then((result) => {
        let url = 'forms/'+id;
        callAjax(url, 'patch', JSON.parse(data), null)
            .done(response => {
                Swal.fire(
                    'Successful!',
                    'Your form has been sended.',
                    'success'
                )
                $('#dataFormSeller').html(response);
            })
            .fail(error => {
                Swal.fire(
                    'Oops!',
                    'Your form hasn`t been sended.',
                    'error'
                );
            })
    })
})

const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
})

$(document).on('click', '.btn-confirm', function (event) {
    let id = $(this).attr('data-id');
    let url = 'forms/'+id;
    swalWithBootstrapButtons.fire({
        title: 'You chose active or reject?',
        text: "Your chose will be send to customer!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Active',
        cancelButtonText: 'Reject',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            let data = '{ "status" : 1}';
            callAjax(url, 'patch', JSON.parse(data), null)
                .done(response => {
                    swalWithBootstrapButtons.fire(
                        'Active!',
                        'The form has been actived.',
                        'success'
                    )
                    $('#dataFormSeller').html(response);
                })
                .fail(error => {
                    Swal.fire(
                        'Oops!',
                        'There was an error in the processing.',
                        'error'
                    );
                })
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            let data = '{ "status" : 2}';
            callAjax(url, 'patch', JSON.parse(data), null)
                .done(response => {
                    swalWithBootstrapButtons.fire(
                        'Active!',
                        'The form has been reject.',
                        'success'
                    )
                    $('#dataFormSeller').html(response);
                })
                .fail(error => {
                    Swal.fire(
                        'Oops!',
                        'There was an error in the processing.',
                        'error'
                    );
                })
        }
    })
})
