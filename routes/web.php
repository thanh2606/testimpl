<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'customer\IndexController@index')->name('index');

Route::get('forbidden', function () {
    return view('forbidden');
})->name('forbidden');

Route::group(['prefix' => 'auth'], function()
{
    Route::get('login', 'LoginController@index')->name('auth.get.login')->middleware('checkRememberToken');
    Route::post('login', 'LoginController@login')->name('auth.post.login')->middleware('checkRememberToken');
    Route::get('logout', 'LoginController@logout')->name('auth.logout');
    Route::get('register', 'LoginController@register')->name('auth.get.register')->middleware('checkLogin');
    Route::post('register', 'LoginController@store')->name('auth.post.register')->middleware('checkLogin');
});

Route::group(['prefix' => 'admin', 'middleware' => ['checkRole', 'checkLogin']], function()
{
    Route::get('dashboard', function () {
        return view('admin.dashboard');
    })->name('admin.dashboard');
    Route::get('seller-manager', function () {
        return view('admin.sellerManager');
    })->name('admin.get.seller');
    Route::get('seller-manager', 'admin\FormController@index')->name('admin.indexForm');
    Route::patch('forms/{form}', 'admin\FormController@update')->name('admin.patch.form');
});

Route::group(['prefix' => 'customer'], function()
{
    Route::get('index', 'customer\IndexController@index')->name('customer.index');
    Route::get('create-register-seller', 'customer\FormController@create')->name('customer.get.form')->middleware('checkLogin');
    Route::get('register-seller', 'customer\FormController@index')->name('customer.indexForm')->middleware('checkLogin');
    Route::post('register-seller', 'customer\FormController@store')->name('customer.post.form')->middleware('checkLogin');
    Route::patch('forms/{form}', 'customer\FormController@update')->name('customer.patch.form')->middleware('checkLogin');
});
